# AWS
## Ejercicio practico:
uso de Lambda, SNS, SQS, DynamoDB

### Features:
	- create-client: carga clientes con dni, nombre, apellido y fecha de nacimiento. Solo debe permitir mayores de edad hasta 65 años
	- create-card: otorga una tarjeta de crédito (Classic si es menor a 45 años y Gold si es mayor) generando número, vencimiento y código de seguridad.
	- create-gift: asigna un regalo de cumpleaños según la estación en la que cae, variando entre buzo si es otoño, sweater si es invierno, camisa si es primavera y remera si es verano.