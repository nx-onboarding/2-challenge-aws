const libDynamoDB = require('aws-sdk/clients/dynamodb');
const libSns = require('aws-sdk/clients/sns');

const db = new libDynamoDB({ region: 'us-east-1' });
const sns = new libSns({ region: 'us-east-1' });

const params = {
  TableName: 'leonardGonzalez-client',
};

const getClient = async (dni) => {
  const client = await db
    .getItem({
      Key: {
        dni: {
          S: dni,
        },
      },
      ...params,
    })
    .promise();
  try {
    return Object.keys(client).length === 0 ? null : client;
  } catch (error) {
    return error;
  }
};

const isInvalidateAge = (date) => {
  const today = new Date();
  const birthDate = new Date(date);
  const age = today.getFullYear() - birthDate.getFullYear();
  return age < 18 || age > 65;
};

const createClient = async (client) => {
  const _params = {
    ...params,
    Item: {
      dni: {
        S: client.dni,
      },
      name: {
        S: client.name,
      },
      lastName: {
        S: client.lastName,
      },
      birthDate: {
        S: client.birthDate,
      },
    },
    ReturnConsumedCapacity: 'TOTAL',
  };

  const snsParams = {
    Message: JSON.stringify(client),
    TopicArn: 'arn:aws:sns:us-east-1:530669888283:leonardGonzalez-clientCreated',
  };

  try {
    if (isInvalidateAge(client.birthDate)) {
      return {
        statusCode: 400,
        body: 'Must be between 18 and 65 years old',
      };
    }
    await db.putItem(_params).promise();

    const snsPublished = await sns.publish(snsParams).promise();
    console.log('snsPublished', snsPublished);
  } catch (error) {
    console.log(error);
    return error;
  }
};

exports.handler = async (event) => {
  if (!event.dni || !event.name || !event.lastName || !event.birthDate) {
    return {
      statusCode: 400,
      body: 'Must include all the items',
    };
  }

  let existClient = await getClient(event.dni);

  if (existClient) {
    return {
      statusCode: 400,
      body: 'Customer already exists',
    };
  }

  let client = await createClient(event);

  if (client?.statusCode) {
    return client;
  }
  try {
    return {
      statusCode: 200,
      body: JSON.stringify('Client successfully registered'),
    };
  } catch (error) {
    const response = {
      statusCode: 500,
      body: 'Unexpected error!',
    };
    return response;
  }
};
