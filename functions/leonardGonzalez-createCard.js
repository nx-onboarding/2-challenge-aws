const libDynamoDB = require('aws-sdk/clients/dynamodb');
const db = new libDynamoDB({ region: 'us-east-1' });

const cardTypeByAge = (date) => {
  const today = new Date();
  const birthDate = new Date(date);
  const age = today.getFullYear() - birthDate.getFullYear();
  return age < 45 ? 'Classic' : 'Gold';
};

const NumberByLength = (length = 16) => {
  return Math.random()
    .toString()
    .substring(2, length + 2);
};

const NumberRnd = (minimum, maximum) => {
  const result = Math.round(Math.random() * (maximum - minimum) + minimum);
  return result < 10 ? `0${result}` : result;
};

exports.handler = async (event) => {
  const body = JSON.parse(event.Records[0].body);

  const { dni, birthDate } = body;

  const typeCard = cardTypeByAge(birthDate);
  const cardNumber = NumberByLength(16);
  const ccvNumber = NumberByLength(3);
  const expirationDate = `${NumberRnd(1, 12)}/${NumberRnd(23, 33)}`;

  const dbParams = {
    TableName: 'leonardGonzalez-client',
    ExpressionAttributeNames: {
      '#C': 'creditCard',
    },
    ExpressionAttributeValues: {
      ':c': {
        M: {
          ccv: {
            S: ccvNumber,
          },
          expirationDate: {
            S: expirationDate,
          },
          cardNumber: {
            S: cardNumber,
          },
          type: {
            S: typeCard,
          },
        },
      },
    },
    Key: {
      dni: {
        S: dni,
      },
    },
    UpdateExpression: 'SET #C = :c',
    ReturnValues: 'ALL_NEW',
  };
  const dbClient = await db.updateItem(dbParams).promise();
  console.log('dbClient', dbClient);

  try {
    return {
      statusCode: 200,
      body: JSON.stringify('Card successfully registered'),
    };
  } catch (error) {
    const response = {
      statusCode: 500,
      body: 'Unexpected error!',
    };
    return response;
  }
};
