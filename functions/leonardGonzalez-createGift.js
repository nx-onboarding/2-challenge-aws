const libDynamoDB = require('aws-sdk/clients/dynamodb');
const db = new libDynamoDB({ region: 'us-east-1' });

const giftByBirthDate = (birthday) => {
  const month = Number(birthday.split('-')[1]);
  if (3 <= month && month <= 5) {
    return 'buzo';
  }

  if (6 <= month && month <= 8) {
    return 'sweater';
  }

  if (9 <= month && month <= 11) {
    return 'camisa';
  }

  return 'remera';
};

exports.handler = async (event) => {
  const body = JSON.parse(event.Records[0].body);
  const { dni, birthDate } = JSON.parse(body.Message);
  const gift = giftByBirthDate(birthDate);

  const dbParams = {
    TableName: 'leonardGonzalez-client',
    ExpressionAttributeNames: {
      '#G': 'gift',
    },
    ExpressionAttributeValues: {
      ':g': {
        S: gift,
      },
    },
    Key: {
      dni: {
        S: dni,
      },
    },
    UpdateExpression: 'SET #G = :g',
    ReturnValues: 'ALL_NEW',
  };
  const dbClient = await db.updateItem(dbParams).promise();
  console.log('dbClient', dbClient);

  try {
    return {
      statusCode: 200,
      body: JSON.stringify('Gift successfully registered'),
    };
  } catch (error) {
    const response = {
      statusCode: 500,
      body: 'Unexpected error!',
    };
    return response;
  }
};
